'use strict';

/**
 * @ngdoc function
 * @name angular10ProjectsApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the angular10ProjectsApp
 */
angular.module('angular10ProjectsApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
