'use strict';

/**
 * @ngdoc function
 * @name angular10ProjectsApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the angular10ProjectsApp
 */
angular.module('angular10ProjectsApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
